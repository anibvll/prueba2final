from django.apps import AppConfig


class ConstructoraappConfig(AppConfig):
    name = 'constructoraApp'
