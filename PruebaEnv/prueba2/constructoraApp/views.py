from django.shortcuts import render
import math


def home(request):
    return render(request,  'constructoraApp/home.html')

def quienes_somos(request):
    return render(request, 'constructoraApp/quienes_somos.html')

def galeria(request):
    return render(request, 'constructoraApp/galeria.html')

def contacto(request):
    return render(request, 'constructoraApp/contacto.html')

def m_medida(request):
    return render(request, 'constructoraApp/m_medida.html')

def m_metraje(request):
    return render(request, 'constructoraApp/m_metraje.html')


