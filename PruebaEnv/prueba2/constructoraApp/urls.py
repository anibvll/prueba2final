from django.urls import path
from .views import home,quienes_somos,contacto,galeria,m_metraje, m_medida
from .views_medida import informacion_medidas
from .views_metraje import informacion_metraje


urlpatterns = [
    path('', home, name = "home"),
    path('quienes_somos/', quienes_somos, name = "quienes_somos"),
    path('galeria/', galeria, name = "galeria"),
    path('contacto/', contacto, name = "contacto"),
    path('m_metraje/', m_metraje, name = "m_metraje"),
    path('m_medida/', m_medida, name= "m_medida"),
    path('m_medida/<int:cant_banio>/<int:cant_habitaciones>', informacion_medidas),
    path('m_metraje/<int:cant_metros_cuadrados>', informacion_metraje)
]   