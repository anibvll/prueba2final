import math
class medida(object):

    def __init__(self, banios,habitaciones):
        self.banios = banios
        self.habitaciones = habitaciones
    
    def get_Banios(self):
        
        return self.banios
    
    def get_Habitaciones(self):
        return self.habitaciones
    
    def Precio_banios(self):
        if self.banios <2 and self.banios >0:

            precio_banios = 1000000 * self.banios
            return precio_banios
    
    def Precio_habitaciones(self):
        if self.habitaciones >0 and self.habitaciones <4:

            precio_habitaciones = 1000000 * self.habitaciones
            return precio_habitaciones
    
    def precio_base(self):
        precio_base = 2000000
        return precio_base

        
    

from django.template.loader import get_template
from django.http import HttpResponse

def informacion_medidas (request,cant_banio, cant_habitaciones):
    obj = medida(cant_banio, cant_habitaciones)
    doc_externo = get_template('m_medida.html')
    documento = doc_externo.render ({'banios': obj.get_Banios, 'habitaciones':obj.get_Habitaciones, 'precio_banios': obj.Precio_banios, "precio_habitaciones": obj.Precio_habitaciones, 'precio_base': obj.precio_base})
    return HttpResponse(documento)